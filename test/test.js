const chai = require('chai');
const expect = chai.expect;
const app = require('../app')
const http = require('chai-http');
const Post = require('../api/post');
chai.use(http);
var postId;

//Basic App Tests
describe('App', () => {
    it('should exist', (done) => {
        expect(app).to.be.a('function');
        done();
    });
    it('should return 200 and message by GET /', (done) => {
        chai.request(app).get('/').then((res) => {
            expect(res).to.have.status(200);
            done();
        }).catch(err => {
            done(err);
            console.log(err.message);
        });
    });
});

describe('Blog Post', function() {
    this.slow(10000);
    it('(Create) should return 201 and have valid title, body, and author', (done) => {
        //mock blog input
        const new_post = {
            "title": "New Title",
            "body": "The author writes down the body of the blog in this part.",
            "author": "Test User"
        }
        
        chai.request(app).post('/post/add').send(new_post).then((res) => {
            expect(res).to.have.status(201);
            expect(res.body.message).to.be.equal("Blog post created");
            expect(res.body.post.title).to.exist;
            expect(res.body.post.body).to.exist;
            expect(res.body.post.author).to.exist;
            expect(res.body.post._id).to.exist;
            expect(res.body.post.createdAt).to.exist;
            done();
        })
        .catch(err=>{
            done(err);
            console.log(err.message);
        });
    });

    //retrieving id of latest blog post
    Post.findOne({}, {}, { sort: { 'createdAt' : -1 } }, function(err, posts) {
       postId = posts._id;
    });
    
    it('(Create Comment) should return 201', (done) => {
        chai.request(app).post('/post/comment/').send({
            id: postId,
            text: 'Comment made today',
            user: 'commenter'
        }).then((res) => {
            expect(res).to.have.status(201);
            expect(res.body.message).to.be.equal("Comment created");
            done();
        }).catch(err=>{
            done(err);
            console.log(err.message);
        });
    });
    
    it('(Read) should return 200 and retrieve documents from collection in database', (done) => {
        chai.request(app).get('/').then((res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.not.equal(null);
            done();
        }).catch(err => {
            done(err);
            console.log(err.message);
        })
    });

    it('(Update) should return 200', (done) => {
        chai.request(app).put('/post/edit/'+postId).send({
            title: 'New Blog Title',
            body: 'New Blog Body',
            author: 'Test User'
        }).then((res) => {
            expect(res).to.have.status(200);
            done();
        }).catch(err=>{
            done(err);
            console.log(err.message);
        });
    });

    it('(Delete) should return 201', (done) => {
        chai.request(app).put('/post/'+postId).then((res) => {
            expect(res).to.have.status(201);
            done();
        }).catch(err=>{
            done(err);
            console.log(err.message);
        });
    });
});