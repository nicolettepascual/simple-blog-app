const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = require('mongodb').ObjectID;
const Post = require('./post');

//creating blog post
router.post('/post/add', (req, res, next) => {
    let hasErrors = false;
    let errors = [];

    if(!req.body.title){
        errors.push({'title': 'Title not received'})
        hasErrors = true;
    }
    if(!req.body.body){
        errors.push({'body': 'Body not received'})
        hasErrors = true
    }
    if(!req.body.author){
        errors.push({'author': 'Author not received'})
        hasErrors = true
    }

    if(hasErrors){
        res.status(422).json({
            message: "Invalid input",
            errors: errors
        });
    }else{
        const postData = new Post({
            _id : mongoose.Types.ObjectId(),
            title: req.body.title,
            body: req.body.body,
            author: req.body.author
        });

        postData.save().then(saved_post => {
            res.status(201).json({
                message: "Blog post created",
                post: saved_post,
                errors: errors
            });
        }).catch(err => {
            errors.push(new Error({
                db: err.message
            }))
            res.status(500).json(errors);
        })
    }
})

//creating a comment
router.post('/post/comment/', (req, res, next) => {
    var postId = req.body.id;
    let hasErrors = false;
    let errors = [];

    if(!req.body.text){
        errors.push({'text': 'text not received'})
        hasErrors = true;
    }
    if(!req.body.user){
        errors.push({'user': 'user not received'})
        hasErrors = true
    }

    if(hasErrors){
        res.status(422).json({
            message: "Invalid input",
            errors: errors
        });
    }else{
        Post.updateOne({"_id": ObjectId(postId)}, {$push: {"comments": {
            text: req.body.text,
            user: req.body.user}
        }}).then(saved_comments => {
            res.status(201).json({
                message: "Comment created",
                post: saved_comments,
                errors: errors
            });
        }).catch(err => {
            errors.push(new Error({
                db: err.message
            }))
            res.status(500).json(errors);
        })
    }
})

//editing a blog post
router.put('/post/edit/:postId', (req, res) => {
    var postId = req.params.postId;
    var title = req.body.title;
    var body = req.body.body;
    let errors = [];
    Post.updateOne({'_id': ObjectId(postId)}, {$set: {title: title, body: body}})
    .then(saved_updates => {
        res.status(200).json({
            message: "Post updated",
            post: saved_updates,
            errors: errors
        });
    }).catch(err => {
        errors.push(new Error({
            db: err.message
        }))
        res.status(500).json(errors);
    })
});

//deleting a blog post
router.put('/post/:postId', (req, res) => {
    let errors = [];
    var postId = req.params.postId;
    Post.deleteOne({'_id': ObjectId(postId)}).then(deleted_post => {
        res.status(201).json({
            message: "Post deleted",
            post: deleted_post,
            errors: errors
        });
    }).catch(err => {
        errors.push(new Error({
            db:err.message
        }))
        res.status(500).json(errors);
    })
})

module.exports = router;