const mongoose = require('mongoose');
const ObjectId = require('mongodb').ObjectID;

const postSchema = new mongoose.Schema({ 
    _id: mongoose.Schema.Types.ObjectId,
    title: {type: String, required: true},
    body: {type: String, required: true },
    author: {type: String, required: true},
    comments: [{
        text: {type: String, required: true},
        user: {type: String},
        timestamps: {type: Date, default: Date.now}
    }]
    },
    {
        timestamps: true
    });
    
module.exports = mongoose.model('Post', postSchema, 'posts');