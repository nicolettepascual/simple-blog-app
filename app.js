// Setup
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const routes = require('./api/routes');
const Post = require('./api/post');

const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true}))

//connecting to database
let dbConn = "mongodb+srv://admin:admin@cluster0-awnkb.mongodb.net/test?retryWrites=true&w=majority" //connection string
    mongoose.set('useCreateIndex', true)
    mongoose.connect(dbConn, {useNewUrlParser: true, useUnifiedTopology: true}).then( () => {
            //console.log('Connected to the database');
          }).catch( err => {
            console.log('Error connecting to the database: ' + err);
            process.exit();
});


//Routes
app.get("/", (req, res) => {
    let blogPosts = [];
    Post.find({}, (err, posts) => {
        res.send(posts);
    });
});

app.use(routes);

module.exports = app;